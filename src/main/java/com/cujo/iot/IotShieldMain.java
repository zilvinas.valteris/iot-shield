package com.cujo.iot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IotShieldMain {

    public static void main(String[] args) {
        new SpringApplication(IotShieldMain.class).run(args);
    }
}
