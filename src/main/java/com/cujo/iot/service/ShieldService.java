package com.cujo.iot.service;

import com.cujo.iot.entity.ProfileCreateEntity;
import com.cujo.iot.entity.ProfileEntity;
import com.cujo.iot.entity.ProfileUpdateEntity;
import com.cujo.iot.entity.RequestEntity;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ShieldService {

    private static final Logger log = LoggerFactory.getLogger(ShieldService.class);
    private static final String BLOCK_POLICY = "block";
    private static final String ALLOW_POLICY = "allow";
    private static final String QUARANTINE_ACTION = "quarantine";
    private static final String BLOCK_ACTION = "block";
    private static final String ALLOW_ACTION = "allow";
    private final Map<String, ProfileEntity> profiles = new HashMap<>();

    public void saveProfile(ProfileCreateEntity profileCreateEntity) {
        ProfileEntity profileEntity = new ProfileEntity();
        profileEntity.setDefaultPolicy(profileCreateEntity.getDefaultPolicy());
        profileEntity.setModelName(profileCreateEntity.getModelName());
        profileEntity.setTimestamp(profileCreateEntity.getTimestamp());
        profileEntity.setBlacklistAddresses(profileCreateEntity.getBlacklistAddresses());
        profileEntity.setWhitelistAddresses(profileCreateEntity.getWhitelistAddresses());

        profiles.put(profileCreateEntity.getModelName(), profileEntity);
    }

    public void updateProfile(ProfileUpdateEntity profileUpdateEntity) {
        ProfileEntity profileToUpdate = profiles.get(profileUpdateEntity.getModelName());

        if (profileToUpdate == null) {
            log.warn("Profile {} does not exist. Therefore its update was not possible.", profileUpdateEntity.getModelName());
            return;
        }

        profileToUpdate.setWhitelistAddresses(profileUpdateEntity.getWhitelistAddresses());
        profileToUpdate.setBlacklistAddresses(profileUpdateEntity.getBlacklistAddresses());
        profileToUpdate.setTimestamp(profileUpdateEntity.getTimestamp());

        profiles.remove(profileUpdateEntity.getModelName());
        profiles.put(profileToUpdate.getModelName(), profileToUpdate);
    }

    public void handleRequest(RequestEntity request) {
        ProfileEntity profileEntity = profiles.get(request.getModelName());

        if (profileEntity == null) {
            log.debug("Could not serve request {} for device model {} sine such profile does not exist",
                      request.getRequestId(),
                      request.getModelName());
            return;
        }

        if (ALLOW_POLICY.equals(profileEntity.getDefaultPolicy())) {
            if (profileEntity.getBlacklistAddresses().contains(request.getUrl())) {
                log.info("request_id: {}, action: {}", request.getRequestId(), BLOCK_ACTION);
                return;
            }
            log.info("request_id: {}, action: {}", request.getRequestId(), ALLOW_ACTION);
            return;
        }

        if (BLOCK_POLICY.equals(profileEntity.getDefaultPolicy())) {
            if (profileEntity.getWhitelistAddresses().contains(request.getUrl())) {
                log.info("request_id: {}, action: {}", request.getRequestId(), ALLOW_ACTION);
                return;
            }
//            log.info("request_id: {}, action: {}", request.getRequestId(), BLOCK_ACTION);
            log.info("device_id: {}, action: {}", request.getRequestId(), QUARANTINE_ACTION);
        }

    }

    public Map<String, ProfileEntity> getProfiles() {
        return profiles;
    }
}
