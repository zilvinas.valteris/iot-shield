package com.cujo.iot.service;

import com.cujo.iot.entity.InputType;
import com.cujo.iot.entity.ProfileCreateEntity;
import com.cujo.iot.entity.ProfileUpdateEntity;
import com.cujo.iot.entity.RequestEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.cujo.iot.entity.AllowedInputTypes.PROFILE_CREATE;
import static com.cujo.iot.entity.AllowedInputTypes.PROFILE_UPDATE;
import static com.cujo.iot.entity.AllowedInputTypes.REQUEST;

@Service
public class InputPacketManager {

    private static final Logger log = LoggerFactory.getLogger(InputPacketManager.class);
    private final ShieldService shieldService;
    private final ObjectMapper objectMapper;

    InputPacketManager(ShieldService shieldService, ObjectMapper objectMapper) {
        this.shieldService = shieldService;
        this.objectMapper = objectMapper;
    }

    public void handle(String input) {
        InputType inputType = getInputType(input);
        if (inputType == null) {
            return;
        }

        if (PROFILE_CREATE.name().equals(inputType.getType())) {
            try {
                ProfileCreateEntity profileCreateEntity = objectMapper.readValue(input, ProfileCreateEntity.class);
                shieldService.saveProfile(profileCreateEntity);
            } catch (JsonProcessingException e) {
                logJsonMappingWarning(input, "ProfileCreateEntity", e.getMessage());
            }
        }
        else if (PROFILE_UPDATE.name().equals(inputType.getType())) {
            try {
                ProfileUpdateEntity profileUpdateEntity = objectMapper.readValue(input, ProfileUpdateEntity.class);
                shieldService.updateProfile(profileUpdateEntity);
            } catch (JsonProcessingException e) {
                logJsonMappingWarning(input, "ProfileUpdateEntity", e.getMessage());
            }
        }
        else if (REQUEST.name().equals(inputType.getType())) {
            try {
                RequestEntity requestEntity = objectMapper.readValue(input, RequestEntity.class);
                shieldService.handleRequest(requestEntity);
            } catch (JsonProcessingException e) {
                logJsonMappingWarning(input, "RequestEntity", e.getMessage());
            }
        }
        else {
            log.info("Input of type {} is not supported - skipping", inputType.getType());
        }
    }

    protected InputType getInputType(String input) {
        InputType inputType = null;
        try {
            inputType = objectMapper.readValue(input, InputType.class);
        } catch (JsonProcessingException e) {
            logJsonMappingWarning(input, "InputType", e.getMessage());
        }
        if (inputType != null) {
            inputType.setType(inputType.getType().toUpperCase());
        }
        return inputType;
    }

    private static void logJsonMappingWarning(String input, String entityName, String errorMessage) {
        log.warn("Was not able to map {} string to {} - skipping; reason: {}", input, entityName, errorMessage);
    }
}
