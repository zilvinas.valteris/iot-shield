package com.cujo.iot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ServiceStarter implements CommandLineRunner {

    private final InputReader inputReader;

    ServiceStarter (InputReader inputReader) {
        this.inputReader = inputReader;
    }

    public void run(String...args) {
        inputReader.readFile();
    }
}
