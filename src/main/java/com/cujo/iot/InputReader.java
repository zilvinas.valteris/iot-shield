package com.cujo.iot;

import com.cujo.iot.service.InputPacketManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.springframework.stereotype.Service;

@Service
public class InputReader {

    private final InputPacketManager inputPacketManager;

    InputReader(InputPacketManager inputPacketManager) {
        this.inputPacketManager = inputPacketManager;
    }

    public void readFile() {
        Scanner scanner;

        try {
            scanner = new Scanner(new File("src/main/resources/input.json")); //TODO: make configurable + handle exceptions properly
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                inputPacketManager.handle(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

}
