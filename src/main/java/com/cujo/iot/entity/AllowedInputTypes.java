package com.cujo.iot.entity;

public enum AllowedInputTypes {
    PROFILE_CREATE,
    PROFILE_UPDATE,
    REQUEST;
}
