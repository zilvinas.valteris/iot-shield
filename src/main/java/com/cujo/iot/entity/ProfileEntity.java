package com.cujo.iot.entity;

import java.util.List;

public class ProfileEntity {

    private String modelName;
    private String defaultPolicy;
    private List<String> whitelistAddresses;
    private List<String> blacklistAddresses;
    private int timestamp;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getDefaultPolicy() {
        return defaultPolicy;
    }

    public void setDefaultPolicy(String defaultPolicy) {
        this.defaultPolicy = defaultPolicy;
    }

    public List<String> getWhitelistAddresses() {
        return whitelistAddresses;
    }

    public void setWhitelistAddresses(List<String> whitelistAddresses) {
        this.whitelistAddresses = whitelistAddresses;
    }

    public List<String> getBlacklistAddresses() {
        return blacklistAddresses;
    }

    public void setBlacklistAddresses(List<String> blacklistAddresses) {
        this.blacklistAddresses = blacklistAddresses;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

}
