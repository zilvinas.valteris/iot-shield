package com.cujo.iot.service;

import com.cujo.iot.entity.ProfileCreateEntity;
import com.cujo.iot.entity.ProfileEntity;
import com.cujo.iot.entity.ProfileUpdateEntity;
import java.util.Collections;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class ShieldServiceTest {

    @InjectMocks
    private ShieldService shieldService;

    @Test
    public void shouldSaveProfilesMapWithProfiles() {
        ProfileCreateEntity profileCreateEntity1 = getDefaultTestProfileCreateEntity();
        ProfileCreateEntity profileCreateEntity2 = getDefaultTestProfileCreateEntity();
        profileCreateEntity2.setModelName("huawei");

        shieldService.saveProfile(profileCreateEntity1);
        shieldService.saveProfile(profileCreateEntity2);

        Map<String, ProfileEntity> profiles = shieldService.getProfiles();

        assertEquals(2, profiles.size());
        assertNotNull(profiles.get("huawei"));
        assertNotNull(profiles.get("iPhone"));
    }

    @Test
    public void shouldUpdateProfilesMap() {
        // create profile for iPhone first
        ProfileCreateEntity profileCreateEntity1 = getDefaultTestProfileCreateEntity();
        shieldService.saveProfile(profileCreateEntity1);

        // make sure profile for iPhone got created
        Map<String, ProfileEntity> profiles = shieldService.getProfiles();
        assertEquals(1, profiles.size());
        assertNotNull(profiles.get("iPhone"));
        assertEquals(getDefaultTestProfileCreateEntity().getWhitelistAddresses(), profiles.get("iPhone").getWhitelistAddresses());

        // update profile
        shieldService.updateProfile(getDefaultTestProfileUpdateEntity());

        // assert entity got updated
        profiles = shieldService.getProfiles();
        assertEquals(1, profiles.size());
        assertEquals(getDefaultTestProfileUpdateEntity().getTimestamp(), profiles.get("iPhone").getTimestamp());
        assertEquals(getDefaultTestProfileUpdateEntity().getWhitelistAddresses().get(0), profiles.get("iPhone").getWhitelistAddresses().get(0));
    }

    @Test
    public void shouldNotUpdateProfilesMapIfNoProfileForGivenModelExists() {
        Map<String, ProfileEntity> profiles = shieldService.getProfiles();

        // make sure the list of profiles is empty to begin with
        assertEquals(0, profiles.size());

        // update profile
        shieldService.updateProfile(getDefaultTestProfileUpdateEntity());

        // assert update has not created a profile
        profiles = shieldService.getProfiles();
        assertEquals(0, profiles.size());
    }

    private static ProfileCreateEntity getDefaultTestProfileCreateEntity() {
        ProfileCreateEntity profileCreateEntity = new ProfileCreateEntity();
        profileCreateEntity.setType("profile_create");
        profileCreateEntity.setModelName("iPhone");
        profileCreateEntity.setDefaultPolicy("block");
        profileCreateEntity.setBlacklistAddresses(Collections.emptyList());
        profileCreateEntity.setWhitelistAddresses(Collections.singletonList("facebook.com"));
        profileCreateEntity.setTimestamp(1562827784);
        return profileCreateEntity;
    }

    private static ProfileUpdateEntity getDefaultTestProfileUpdateEntity() {
        ProfileUpdateEntity profileUpdateEntity = new ProfileUpdateEntity();
        profileUpdateEntity.setType("profile_update");
        profileUpdateEntity.setModelName("iPhone");
        profileUpdateEntity.setBlacklistAddresses(Collections.emptyList());
        profileUpdateEntity.setWhitelistAddresses(Collections.singletonList("delfi.lt"));
        profileUpdateEntity.setTimestamp(1562828784);
        return profileUpdateEntity;
    }

}
