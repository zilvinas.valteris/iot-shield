package com.cujo.iot.service;

import com.cujo.iot.entity.InputType;
import com.cujo.iot.entity.ProfileCreateEntity;
import com.cujo.iot.entity.ProfileUpdateEntity;
import com.cujo.iot.entity.RequestEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InputPacketManagerTest {

    @InjectMocks
    private InputPacketManager inputPacketManager;

    @Mock
    private ShieldService shieldService;

    @Mock
    private ObjectMapper objectMapper;

    private static final String profileCreate = "{\"type\": \"profile_create\", \"model_name\": \"iPhone\", \"default_policy\": " +
            "\"block\", \"whitelist\": [\"facebook.com\"], \"blacklist\": [], \"timestamp\": 1562827784}";

    private static final String profileUpdate = "{\"type\": \"profile_update\", \"model_name\": \"iPhone\", \"whitelist\": " +
            "[\"other_site.com\"], \"timestamp\": 1562827984}";

    private static final String request = "{\"type\": \"request\", \"request_id\": \"66d03a6947c048009f0b34260f35f3bd\", " +
            "\"model_name\": \"iPhone\", \"device_id\": \"7fcbe1dff23947e5b3db8a20c1a2f8c0\", \"url\": \"facebook" +
            ".com\", \"timestamp\": 1562827794}";

    @Test
    public void shouldCallSaveMethodWhenProfileCreateJsonReceived() throws JsonProcessingException {
        InputType inputType = new InputType();
        inputType.setType("profile_create");

        when(objectMapper.readValue(profileCreate, InputType.class)).thenReturn(inputType);
        when(objectMapper.readValue(profileCreate, ProfileCreateEntity.class)).thenReturn(new ProfileCreateEntity());

        inputPacketManager.handle(profileCreate);

        verify(shieldService,times(1)).saveProfile(any(ProfileCreateEntity.class));
    }

    @Test
    public void shouldCallUpdateMethodWhenProfileCreateJsonReceived() throws JsonProcessingException {
        InputType inputType = new InputType();
        inputType.setType("profile_update");

        when(objectMapper.readValue(profileUpdate, InputType.class)).thenReturn(inputType);
        when(objectMapper.readValue(profileUpdate, ProfileUpdateEntity.class)).thenReturn(new ProfileUpdateEntity());

        inputPacketManager.handle(profileUpdate);

        verify(shieldService,times(1)).updateProfile(any(ProfileUpdateEntity.class));
    }

    @Test
    public void shouldCallHandleRequestMethodWhenProfileCreateJsonReceived() throws JsonProcessingException {
        InputType inputType = new InputType();
        inputType.setType("request");

        when(objectMapper.readValue(request, InputType.class)).thenReturn(inputType);
        when(objectMapper.readValue(request, RequestEntity.class)).thenReturn(new RequestEntity());

        inputPacketManager.handle(request);

        verify(shieldService,times(1)).handleRequest(any(RequestEntity.class));
    }

    @Test
    public void shouldFallThroughIfInputTypeIsNotRecognised() {
        String unknownInput = "{\"type\": \"lost_and_confused\", \"model_name\": \"iPhone\", \"whitelist\": " +
                "[\"other_site.com\"], \"timestamp\": 1562827984}";

        inputPacketManager.handle(unknownInput);

        verify(shieldService, never()).saveProfile(any());
        verify(shieldService, never()).updateProfile(any());
        verify(shieldService, never()).handleRequest(any());
    }

    @Test
    public void shouldCallNothingWhenCannotMapTypeDueToException() {
        String invalidJson = "{\"type\": invalid_jason, \"model_name\": \"iPhone\", \"whitelist\": " +
                "[\"other_site.com\"], \"timestamp\": 1562827984}";

        inputPacketManager.handle(invalidJson);

        verify(shieldService, never()).saveProfile(any());
        verify(shieldService, never()).updateProfile(any());
        verify(shieldService, never()).handleRequest(any());
    }
}
