package com.cujo.iot.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EntityMappingIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldMapInputTypeToObjectWhenJsonIsValid() throws JsonProcessingException {
        String anyInput = "{\"type\": \"some_type\", \"model_name\": \"iPhone\", \"default_policy\": " +
                "\"block\", \"whitelist\": [\"facebook.com\"], \"blacklist\": [], \"timestamp\": 1562827784}";

        InputType inputType = objectMapper.readValue(anyInput, InputType.class);

        assertEquals("some_type", inputType.getType());
    }

    @Test
    public void shouldMapProfileCreateInputStringToObjectWhenJsonIsValid() throws JsonProcessingException {
        String profileCreateInput = "{\"type\": \"profile_create\", \"model_name\": \"iPhone\", \"default_policy\": " +
                "\"block\", \"whitelist\": [\"facebook.com\"], \"blacklist\": [], \"timestamp\": 1562827784}";

        ProfileCreateEntity profileCreateEntity = objectMapper.readValue(profileCreateInput, ProfileCreateEntity.class);

        assertEquals("profile_create", profileCreateEntity.getType());
        assertEquals("iPhone", profileCreateEntity.getModelName());
        assertEquals("block", profileCreateEntity.getDefaultPolicy());
        assertEquals("facebook.com", profileCreateEntity.getWhitelistAddresses().get(0));
        assertEquals(0, profileCreateEntity.getBlacklistAddresses().size());
        assertEquals(1562827784, profileCreateEntity.getTimestamp());
    }

    @Test
    public void shouldIgnoreUnexpectedPropertiesWhenMappingProfileCreateStringToObject() throws JsonProcessingException {
        String profileCreateInput = "{\"type\": \"profile_create\", \"model_name\": \"iPhone\", \"default_policy\": " +
                "\"block\", \"whitelist\": [\"facebook.com\"], \"blacklist\": [], \"timestamp\": 1562827784, " +
                "\"unexpected_property\":  \"hello there\"}";

        ProfileCreateEntity profileCreateEntity = objectMapper.readValue(profileCreateInput, ProfileCreateEntity.class);

        assertEquals("profile_create", profileCreateEntity.getType());
        assertEquals("iPhone", profileCreateEntity.getModelName());
        assertEquals("block", profileCreateEntity.getDefaultPolicy());
        assertEquals("facebook.com", profileCreateEntity.getWhitelistAddresses().get(0));
        assertEquals(0, profileCreateEntity.getBlacklistAddresses().size());
        assertEquals(1562827784, profileCreateEntity.getTimestamp());
    }

    @Test
    public void shouldMapProfileUpdateInputStringToObjectWhenJsonIsValid() throws JsonProcessingException {
        String profileCreateInput = "{\"type\": \"profile_update\", \"model_name\": \"iPhone\", \"whitelist\": " +
                "[\"other_site.com\"], \"timestamp\": 1562827984}";

        ProfileUpdateEntity profileUpdateEntity = objectMapper.readValue(profileCreateInput, ProfileUpdateEntity.class);

        assertEquals("profile_update", profileUpdateEntity.getType());
        assertEquals("iPhone", profileUpdateEntity.getModelName());
        assertEquals("other_site.com", profileUpdateEntity.getWhitelistAddresses().get(0));
        assertEquals(1562827984, profileUpdateEntity.getTimestamp());
    }

    @Test
    public void shouldMapRequestInputStringToObjectWhenJsonIsValid() throws JsonProcessingException {
        String profileCreateInput = "{\"type\": \"request\", \"request_id\": \"66d03a6947c048009f0b34260f35f3bd\", " +
                "\"model_name\": \"iPhone\", \"device_id\": \"7fcbe1dff23947e5b3db8a20c1a2f8c0\", \"url\": \"facebook" +
                ".com\", \"timestamp\": 1562827794}";

        RequestEntity requestEntity = objectMapper.readValue(profileCreateInput, RequestEntity.class);

        assertEquals("request", requestEntity.getType());
        assertEquals("66d03a6947c048009f0b34260f35f3bd", requestEntity.getRequestId());
        assertEquals("iPhone", requestEntity.getModelName());
        assertEquals("7fcbe1dff23947e5b3db8a20c1a2f8c0", requestEntity.getDeviceId());
        assertEquals("facebook.com", requestEntity.getUrl());
        assertEquals(1562827794, requestEntity.getTimestamp());
    }

}
